# syntax=docker/dockerfile:1

# builder image: generate a base debian directory
# ----------------------------------------------
FROM debian:stable as builder
RUN apt-get update && apt-get install -y debootstrap && apt-get clean

WORKDIR /root
RUN debootstrap --foreign --no-check-gpg --variant minbase --arch armhf \
                stable fs http://mirrordirector.raspbian.org/raspbian

# save ownership of non-root and files with special permissions (suid, sgid, sticky)
WORKDIR /root/fs
RUN stat -c "chown %u:%g %n" $(find . ! -user root -o ! -group root) > .non-root.sh && \
    stat -c "chmod %a %n" $(find . -perm /7000) >> .non-root.sh && \
    chmod +x .non-root.sh

# layered image: build on this base debian directory
# --------------------------------------------------
FROM scratch as layered
WORKDIR /

# Copy the subdirectory generated from debootstrap first stage
COPY --from=builder /root/fs .

# We want ARM CPU emulation to work, even if we are running all this
# on the docker hub (automated build) and binfmt_misc is not available.
# So we get the modified qemu built by guys from resin.io.
# This modified qemu is able to catch subprocesses creation and handle
# their CPU emulation, when called with option '-execve'.
# (https://resin.io/blog/building-arm-containers-on-any-x86-machine-even-dockerhub/)
COPY --from=resin/armv7hf-debian-qemu /usr/bin/qemu-arm-static /usr/bin/

# let docker build process call qemu-arm-static
SHELL ["/usr/bin/qemu-arm-static", "-execve", "/bin/sh", "-c"]

# Restore ownership of non-root files that may have been lost during copy
RUN sh .non-root.sh

# second stage of debootstrap will try to mount things already mounted,
# do not fail
RUN ln -sf /bin/true /bin/mount

# call second stage of debootstrap
RUN /debootstrap/debootstrap --second-stage
RUN apt-get clean

# set locale
#RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
#RUN locale-gen en_US.UTF-8
#RUN update-locale en_US.UTF-8

# update package repositories
ADD etc/apt/sources.list /etc/apt/sources.list

# import from ubuntu keyserver because keypool one is deprecated
RUN gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-key 82B129927FA3303E
RUN gpg -a --export 82B129927FA3303E | apt-key add -
RUN gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-key 8B48AD6246925553
RUN gpg -a --export 8B48AD6246925553 | apt-key add -
RUN gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-key 7638D0442B90D010
RUN gpg -a --export 7638D0442B90D010 | apt-key add -

# gpg keyrings
ADD usr/share/keyrings/debian-7-wheezy-archive-keyring.gpg /usr/share/keyrings/
ADD usr/share/keyrings/debian-8-jessie-archive-keyring.gpg /usr/share/keyrings/
ADD usr/share/keyrings/debian-torproject-archive-keyring.gpg /usr/share/keyrings/
ADD usr/share/keyrings/raspberrypi-archive-keyring.gpg /usr/share/keyrings/
ADD usr/share/keyrings/thetorproject-archive-keyring.gpg /usr/share/keyrings/

# full update with new repositories/keys to have secure system
RUN apt install -y apt-transport-https ca-certificates
RUN apt update
RUN apt upgrade
RUN apt dist-upgrade
RUN apt full-upgrade

# install required softwares
RUN apt install -y build-essential git
RUN apt install -y tor nginx

RUN touch /opt/toxcash && echo "toxcash:$USER" > /opt/toxcash

# copy tor config file
ADD etc/nginx/nginx.conf /etc/nginx/nginx.conf
ADD etc/nginx/sites-enabled/serve-files /etc/nginx/sites-enabled/
ADD etc/tor/torrc /etc/tor/torrc

# setup tor service
RUN service nginx start
RUN service tor start
# RUN service nginx status >> /opt/nginx-start.log >/dev/null
# RUN service tor status >> /opt/tor-start.log >/dev/null

# save ownership of non-root and files with special permissions (suid, sgid, sticky)
RUN stat -c "ls -ld %n; chown %u:%g %n" $(find . -xdev ! -user root -o ! -group root) > .non-root.sh && \
    stat -c "ls -ld %n; chmod %a %n" $(find . -xdev -perm /7000) >> .non-root.sh && \
    chmod +x .non-root.sh

# squashed image: compress layered image
# --------------------------------------
# We will squash layers into a clean image.
# First stage of debootstrap have created many files that were actually
# removed by the second stage, so our final image can be made at least
# twice smaller than previous one ("layered").
FROM scratch

COPY --from=layered / /

# Restore ownership of non-root files that may have been lost during copy
SHELL ["/usr/bin/qemu-arm-static", "-execve", "/bin/bash", "-c"]
RUN sh .non-root.sh && rm .non-root.sh
CMD ["/bin/bash"]
