#!/bin/bash

openssl genpkey -algorithm x25519 -out /tmp/k1.prv.pem
cat /tmp/k1.prv.pem | grep -v " PRIVATE KEY" | base64pem -d | tail -c 32 | base32 | sed 's/=//g' > /tmp/k1.prv.key
openssl pkey -in /tmp/k1.prv.pem -pubout | grep -v " PUBLIC KEY" | base64pem -d | tail -c 32 | base32 | sed 's/=//g' > /tmp/k1.pub.key

echo "Server token:"
echo "/var/lib/tor/hidden_service/authorized_clients/<name>.auth:"
echo "description:x25519:$(cat /tmp/k1.pub.key)"
echo ""
echo "Client token:"
echo "<ClientOnionAuthDir>/<name>.auth_private:"
echo "<onion_pub_key>:description:x25519:$(cat /tmp/k1.prv.key)"

rm -f /tmp/k1.pub.key /tmp/k1.prv.key /tmp/k1.prv.pem
