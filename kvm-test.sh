#!/bin/bash

# test it with kvm
cp tox_rpi_stretch.img tox_rpi_stretch.test.img;
truncate -s 2G tox_rpi_stretch.test.img; # simulate copy on 2G usb key
kvm -m 1024 -hda tox_rpi_stretch.test.img;
