echo "cleaning up previously built distribution files..."

. cleanup.sh

echo "cleanup done!"

echo "building docker image..."

docker build \
  --network "host" \
  --tag toxcash:latest \
. -f Dockerfile;

echo "docker image built!"

echo "starting docker image..."

docker run \
  --name toxcash_container \
  --entrypoint /bin/bash \
  toxcash:latest;

echo "docker image started!"

echo "entering the toxcash filesystem directory..."

mkdir toxcash_fs;
cd toxcash_fs;

echo " => exporting the distribution..."

docker export toxcash_container | gzip > ../toxcash_latest.tar.gz;

echo " => exported toxcash distribution in toxcash_latest.tar.gz!"

echo " => removing toxcash_container..."
docker rm toxcash_container >/dev/null
echo " => toxcash container removed!"

cd ..;
rm -rf toxcash_fs;

echo "back to project root folder!"

echo "toxcash dockerized export done"
echo "Run ./debootstick.sh using the .tar.gz archive in a debian environment to obtain a burnable image"
